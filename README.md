# Test API

[API1](#api1)

[API2](#api2)

[API3](#api3)

[API4](#api4)

[API5](#api5)


### API1

##### URL
> `https://ding.api.org/api/ding`

##### Method
> `POST`

##### Parameters
> 
| Name | Nonnull | Type | Description |
| ---- | :---: | ---- | ----|
| account | * | String | User Name |
| password | * | String | Password |

##### Result
> ```json
{
    "result": 200,
    "dingId": "df234-dfwj238-fdfadDF-2138fd"
}
```

***

### API2

##### URL
> `https://ding.api.org/api/ding`

##### Method
> `POST`

##### Parameters
> 
| Name | Nonnull | Type | Description |
| ---- | :---: | ---- | ----|
| account | * | String | User Name |
| password | * | String | Password |

##### Result
> ```json
{
    "result": 200,
    "dingId": "df234-dfwj238-fdfadDF-2138fd"
}
```

***


### API1

##### URL
> `https://ding.api.org/api/ding`

##### Method
> `POST`

##### Parameters
> 
| Name | Nonnull | Type | Description |
| ---- | :---: | ---- | ----|
| account | * | String | User Name |
| password | * | String | Password |

##### Result
> ```json
{
    "result": 200,
    "dingId": "df234-dfwj238-fdfadDF-2138fd"
}
```

***

### API3

##### URL
> `https://ding.api.org/api/ding`

##### Method
> `POST`

##### Parameters
> 
| Name | Nonnull | Type | Description |
| ---- | :---: | ---- | ----|
| account | * | String | User Name |
| password | * | String | Password |

##### Result
> ```json
{
    "result": 200,
    "dingId": "df234-dfwj238-fdfadDF-2138fd"
}
```

***

### API4

##### URL
> `https://ding.api.org/api/ding`

##### Method
> `POST`

##### Parameters
> 
| Name | Nonnull | Type | Description |
| ---- | :---: | ---- | ----|
| account | * | String | User Name |
| password | * | String | Password |

##### Result
> ```json
{
    "result": 200,
    "dingId": "df234-dfwj238-fdfadDF-2138fd"
}
```

***

### API5

##### URL
> `https://ding.api.org/api/ding`

##### Method
> `POST`

##### Parameters
> 
| Name | Nonnull | Type | Description |
| ---- | :---: | ---- | ----|
| account | * | String | User Name |
| password | * | String | Password |

##### Result
> ```json
{
    "result": 200,
    "dingId": "df234-dfwj238-fdfadDF-2138fd"
}
```

***
